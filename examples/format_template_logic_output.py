#!/usr/bin/python
#
# This script takes the output from example_template_logic.cpp after it has
# passed through 'c++filt -t' and formats it in a human-readable way. Most of
# the string replacement is specialized to the types used in the example, but
# it should continue working for small modifications, especially if only the
# query is changed.

import fileinput
for line in fileinput.input():
    result = line.strip().replace("cex::template_logic::", "")      \
                         .replace(", global_scope",        "")      \
                         .replace("ul",                    "")      \
                         .replace("symbol<48>",            "Zero")  \
                         .replace("variable<88>",          "X")     \
                         .replace("variable<89>",          "Y")     \
                         .replace(" >",                    ">")     \
                         .replace("s<Zero>",               "One")   \
                         .replace("s<One>",                "Two")   \
                         .replace("s<Two>",                "Three") \
                         .replace("s<Three>",              "Four")  \
                         .replace("s<Four>",               "Five")  \
                         .replace("s<Five>",               "Six")   \
                         .replace("s<Six>",                "Seven") \
                         .replace("s<Seven>",              "Eight") \
                         .replace("s<Eight>",              "Nine")  \
                         .replace("s<Nine>",               "Ten")   \
                         .replace("s<Ten>",                "Eleven")
    indent = 0
    output = ""

    # Formatting
    for char in result:
        if char == "<":
            indent += 2
            output += "<\n" + " " * indent
        elif char == ">":
            indent -= 2
            output += "\n" + " " * indent + ">"
        elif char == " ":
            output += "\n" + " " * indent
        else:
            output += char
    
    # local_scope tags
    # These should never appear when outputting the results, only if intermediate
    # types are inspected.
    while "local_scope" in output:
        balance = 1
        start = output.find("local_scope")
        position = start + len("local_scope") + 1 # Skip <
        while balance > 0:
            if output[position] == "<":
                balance += 1
            elif output[position] == ">":
                balance -= 1
            position += 1
        substring = output[start:position]
        depth = substring.count("local_scope") - 1
        output = output[:start] + "(local variable at depth " + str(depth) \
                                + ")" + output[position:]
    
    print(output)
