#include <iostream>
#include <vector>
#include <utility>

#include <cex/meta_vector.hpp>

// Some example meta_vectors
using meta = cex::meta_vector<int, long, std::string, const char*, std::pair<int, int>, std::vector<int>, std::ostream>;
using simple = cex::meta_vector<int, long>;

// Dump the vector to test cex::at
template <long L> void _test_at()
{
    if constexpr (L > 0) _test_at<L - 1>();
    std::cout << "cex::at (at " << L << "):    " << typeid(typename cex::at<meta, L>).name() << std::endl;
}
void test_at() { _test_at<meta::size - 1>(); }

// Test other API stuff
void test_api() {
  std::cout << "cex::front:        " << typeid(typename cex::front<simple>).name() << std::endl;
  std::cout << "cex::back:         " << typeid(typename cex::back<simple>).name() << std::endl;
  std::cout << "cex::clear:        " << typeid(typename cex::clear<simple>).name() << std::endl;
  std::cout << "cex::merge:        " << typeid(typename cex::merge<simple, simple>).name() << std::endl;
  std::cout << "cex::slice:        " << typeid(typename cex::slice<simple, 1, 2>).name() << std::endl;
  std::cout << "cex::insert_range: " << typeid(typename cex::insert_range<simple, 1, simple>).name() << std::endl;
  std::cout << "cex::insert:       " << typeid(typename cex::insert<simple, 1, float, double>).name() << std::endl;
  std::cout << "cex::erase:        " << typeid(typename cex::erase<simple, 1>).name() << std::endl;
  std::cout << "cex::erase:        " << typeid(typename cex::erase<simple, 0, 2>).name() << std::endl;
  std::cout << "cex::push_back:    " << typeid(typename cex::push_back<simple, float, double>).name() << std::endl;
  std::cout << "cex::push_front:   " << typeid(typename cex::push_front<simple, float, double>).name() << std::endl;
  std::cout << "cex::pop_back:     " << typeid(typename cex::pop_back<simple>).name() << std::endl;
  std::cout << "cex::pop_front:    " << typeid(typename cex::pop_front<simple>).name() << std::endl;
}

// Test cex API
int main()
{
    test_at();
    test_api();
}