#include <iostream>

#include <cex/template_logic.hpp>

namespace ctl = cex::template_logic;

// Variables and symbols
using X    = ctl::variable<'X'>;
using Y    = ctl::variable<'Y'>;
using Zero = ctl::symbol<'0'>;

// Predicates
template <typename... Args> struct s   {}; // Integers (via s<s<Zero>>)
template <typename... Args> struct add {}; // Integer addition

// Rules
//  add(s(X), Y, s(Z)) :- add(X, Y, Z)
//  add(Zero, Y, Y).
template <typename A, typename B, typename C>
struct add<A, B, C>
    : ctl::rules<
        ctl::rule<
            add<ctl::local<0>, ctl::local<1>, ctl::local<2>>,
            ctl::equation<s<ctl::local<0>>, A>,
            ctl::equation<  ctl::local<1> , B>,
            ctl::equation<s<ctl::local<2>>, C>
        >,
        ctl::rule<
            ctl::success,
            ctl::equation<Zero,          A>,
            ctl::equation<ctl::local<0>, B>,
            ctl::equation<ctl::local<0>, C>
        >
      >
{};

// Aliases
using One    = s<Zero>;
using Two    = s<s<Zero>>;
using Three  = s<s<s<Zero>>>;
using Four   = s<s<s<s<Zero>>>>;
using Five   = s<s<s<s<s<Zero>>>>>;
using Six    = s<s<s<s<s<s<Zero>>>>>>;
using Seven  = s<s<s<s<s<s<s<Zero>>>>>>>;
using Eight  = s<s<s<s<s<s<s<s<Zero>>>>>>>>;
using Nine   = s<s<s<s<s<s<s<s<s<Zero>>>>>>>>>;
using Ten    = s<s<s<s<s<s<s<s<s<s<Zero>>>>>>>>>>;
using Eleven = s<s<s<s<s<s<s<s<s<s<s<Zero>>>>>>>>>>>;

// Query
using query = add<X, Y, Seven>;

// Test
template <typename T> void print_type() { std::cout << typeid(T).name() << std::endl; }
int main()
{
    print_type<ctl::solve<query>>();
}
