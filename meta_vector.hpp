#ifndef CEX_META_VECTOR_HPP_INCLUDED
#define CEX_META_VECTOR_HPP_INCLUDED

namespace cex
{
namespace detail
{

// A simple wrapper storing its template argument
template <typename T> struct wrapper { using type = T; };

// This helper class inherits a 'type_at' function for each index behind the current position
template <size_t Index, typename... Args> struct vector_helper;

template <size_t Index, typename Type, typename... Remainder>
struct vector_helper<Index, Type, Remainder...> : vector_helper<Index + 1, Remainder...>
{
    static wrapper<Type> type_at(std::integral_constant<size_t, Index>);
    using vector_helper<Index + 1, Remainder...>::type_at;
};
template <size_t Index> struct vector_helper<Index>
{
    static void type_at();
};

// This helper class slices the first N items of a template parameter pack
template <template <typename...> typename W, typename Full, size_t N, typename Results = W<>, typename = void> struct slice_helper;
template <template <typename...> typename W, typename T, typename... Ts, size_t N, typename... Results>
struct slice_helper<W, W<T, Ts...>, N, W<Results...>, std::enable_if_t<N != 0>> : slice_helper<W, W<Ts...>, N - 1, W<Results..., T>> {};
template <template <typename...> typename W, typename... Ts, typename... Results>
struct slice_helper<W, W<Ts...>, 0, W<Results...>, void> { using type = W<Results...>; };

} // namespace detail

// A vector of types
template <typename... Types>
struct meta_vector : public ::cex::detail::vector_helper<0, Types...>
{
    static constexpr auto size = sizeof...(Types);
};

// Metadata
template <typename Collection>                               constexpr size_t size = 0;
template <template <typename...> typename W, typename... Ts> constexpr size_t size<W<Ts...>> = sizeof...(Ts);

template <typename Collection> constexpr bool empty = size<Collection> == 0;


namespace ops
{

// Operations
template <typename Collection, size_t Index, typename = void> struct at_s;
template <typename... Ts, size_t Index>
struct at_s<meta_vector<Ts...>, Index, void>
{
  using type = typename decltype(meta_vector<Ts...>::type_at(std::integral_constant<size_t, Index>{}))::type;
};
template <template <typename...> typename W, typename T, typename... Ts, size_t Index>
struct at_s<W<T, Ts...>, Index, std::enable_if_t<!std::is_same_v<W, meta_vector>>> : at_s<W<Ts...>, Index - 1> {};
template <template <typename...> typename W, typename T, typename... Ts>
struct at_s<W<T, Ts...>, 0, std::enable_if_t<!std::is_same_v<W, meta_vector>>> { using type = T; };

template <typename Collection> struct clear_s;
template <template <typename...> typename W, typename... Ts> struct clear_s<W<Ts...>> { using type = W<>; };

template <typename Coll1, typename Coll2> struct merge_s;
template <template <typename...> typename W, typename... Ts, typename... Us> struct merge_s<W<Ts...>, W<Us...>> { using type = W<Ts..., Us...>; };

template <typename Collection, size_t Begin, size_t End, typename = void> struct slice_s;
template <template <typename...> typename W, typename T, typename... Ts, size_t Begin, size_t End>
struct slice_s<W<T, Ts...>, Begin, End, std::enable_if_t<Begin != End && Begin != 0>> : slice_s<W<Ts...>, Begin - 1, End - 1> {};
template <template <typename...> typename W, typename T, typename... Ts, size_t End>
struct slice_s<W<T, Ts...>, 0, End, std::enable_if_t<End != 0>> : ::cex::detail::slice_helper<W, W<T, Ts...>, End> {};
template <template <typename...> typename W, typename... Ts, size_t Same>
struct slice_s<W<Ts...>, Same, Same, void> { using type = W<>; };

template <typename Collection, typename... Args> struct assign_s;
template <template <typename...> typename W, typename... Ts, typename... Args> struct assign_s<W<Ts...>, Args...> { using type = W<Args...>; };

template <typename Coll1, size_t Index, typename Coll2> class insert_range_s
{
private:
    using pre_slice  = typename slice_s<Coll1, 0, Index>::type;
    using post_slice = typename slice_s<Coll1, Index, size<Coll1>>::type;
    using temporary  = typename merge_s<pre_slice, Coll2>::type;
public:
    using type       = typename merge_s<temporary, post_slice>::type;
};
template <typename Collection, size_t Index, typename... Ts> struct insert_s : insert_range_s<Collection, Index, typename assign_s<Collection, Ts...>::type> {};

template <typename Collection, size_t Begin, size_t End = Begin + 1> class erase_s
{
private:
    using pre_slice  = typename slice_s<Collection, 0, Begin>::type;
    using post_slice = typename slice_s<Collection, End, size<Collection>>::type;
public:
    using type       = typename merge_s<pre_slice, post_slice>::type;
};

} // namespace ops

// Operation aliases
template <typename Collection, size_t Index>                         using at           = typename ::cex::ops::at_s<Collection, Index>::type;
template <typename Collection>                                       using front        = at<Collection, 0>;
template <typename Collection>                                       using back         = at<Collection, std::max((size_t) 0, size<Collection> - 1)>;
template <typename Collection>                                       using clear        = typename ::cex::ops::clear_s<Collection>::type;
template <typename Coll1, typename Coll2>                            using merge        = typename ::cex::ops::merge_s<Coll1, Coll2>::type;
template <typename Collection, size_t Begin, size_t End>             using slice        = typename ::cex::ops::slice_s<Collection, Begin, End>::type;
template <typename Coll1, size_t Index, typename Coll2>              using insert_range = typename ::cex::ops::insert_range_s<Coll1, Index, Coll2>::type;
template <typename Collection, size_t Index, typename... Ts>         using insert       = typename ::cex::ops::insert_s<Collection, Index, Ts...>::type;
template <typename Collection, size_t Begin, size_t End = Begin + 1> using erase        = typename ::cex::ops::erase_s<Collection, Begin, End>::type;
template <typename Collection, typename... Ts>                       using push_back    = insert<Collection, size<Collection>, Ts...>;
template <typename Collection, typename... Ts>                       using push_front   = insert<Collection, 0, Ts...>;
template <typename Collection>                                       using pop_back     = erase<Collection, size<Collection> - 1>;
template <typename Collection>                                       using pop_front    = erase<Collection, 0>;

} // namespace cex

// cex::meta_vector / boost::mpl::vector compatability layer ('begin', 'end', and 'insert_range' are not implemented here, because they rely on MPL iterators)
#ifndef CEX_NO_BOOST_MPL_COMPAT
#include <boost/mpl/vector.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/mpl/erase.hpp>

namespace boost::mpl
{

#define COMPAT_META ::cex::meta_vector<Ts...>
template <typename... Ts>                             struct size<COMPAT_META>            { using type = boost::mpl::long_<::cex::size<COMPAT_META>>; };
template <typename... Ts>                             struct empty<COMPAT_META>           { using type = boost::mpl::bool_<::cex::empty<COMPAT_META>>; };
template <typename... Ts>                             struct front<COMPAT_META>           { using type = ::cex::front<COMPAT_META>; };
template <typename... Ts>                             struct back<COMPAT_META>            { using type = ::cex::back<COMPAT_META>; };
template <typename... Ts, typename Pos>               struct at<COMPAT_META, Pos>         { using type = ::cex::at<COMPAT_META, Pos::value>; };
template <typename... Ts, typename Pos, typename X>   struct insert<COMPAT_META, Pos, X>  { using type = ::cex::insert<COMPAT_META, Pos::value, X>; };
template <typename... Ts, typename Beg, typename End> struct erase<COMPAT_META, Beg, End> { using type = ::cex::erase<COMPAT_META, Beg::value, End::value>; };
template <typename... Ts>                             struct clear<COMPAT_META>           { using type = ::cex::clear<COMPAT_META>; };
template <typename... Ts, typename X>                 struct push_back<COMPAT_META, X>    { using type = ::cex::push_back<COMPAT_META, X>; };
template <typename... Ts>                             struct pop_back<COMPAT_META>        { using type = ::cex::pop_back<COMPAT_META>; };
template <typename... Ts, typename X>                 struct push_front<COMPAT_META, X>   { using type = ::cex::push_front<COMPAT_META, X>; };
template <typename... Ts>                             struct pop_front<COMPAT_META>       { using type = ::cex::pop_front<COMPAT_META>; };
#undef COMPAT_META

} // namespace boost::mpl

#endif // CEX_NO_BOOST_MPL_COMPAT

#endif // CEX_META_VECTOR_HPP_INCLUDED
