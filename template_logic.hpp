#ifndef CEX_TEMPLATE_LOGIC_HPP_INCLUDED
#define CEX_TEMPLATE_LOGIC_HPP_INCLUDED

#include <cstdint>
#include <type_traits>

namespace cex::template_logic
{

// Wrapper for a set of multiple types (we really could use cex::meta_vector here, but this is a one-off experiment, so I'll leave it as-is)
template <typename... Items> struct set { constexpr static size_t size = sizeof...(Items); };

// Scopes
struct global_scope {};
template <typename PredicateInstance> struct local_scope {};

// Symbols and variables
template <std::size_t Key> struct symbol {};
template <std::size_t Key, typename Scope = global_scope> struct variable {};
template <std::size_t Key> struct local {}; // This is a local variable placeholder that should be substituted with variable<Key, Scope> once the proper Scope is determined

// Equations
template <typename Left, typename Right> struct equation { using left = Left; using right = Right; };

// Results
struct success {};
struct failure {};

// Basic terms
template <typename... Terms> struct and_ {};
template <typename... Terms> struct or_ {};
template <typename Term> struct not_ {};

// Template joining
template <typename... Args> struct join;
template <typename T> struct join<T> { using type = T; };
template <template <typename...> typename Base, typename... A, typename... B> struct join<Base<A...>, Base<B...>> { using type = Base<A..., B...>; };
template <template <typename...> typename Base, typename... A, typename... B, typename... Remainder> struct join<Base<A...>, Base<B...>, Remainder...> : join<Base<A..., B...>, Remainder...> {};

// Extract last template argument
template <typename Stored, typename... Remainder> struct last_helper;
template <typename... Previous, typename T> struct last_helper<set<Previous...>, T> { using type = T; using head = set<Previous...>; };
template <typename... Previous, typename T, typename... Remainder> struct last_helper<set<Previous...>, T, Remainder...> : last_helper<set<Previous..., T>, Remainder...> {};
template <typename... Args> struct last : last_helper<set<>, Args...> {};

// Check if a term contains a variable or other type
template <typename Target, typename Data> struct contains : std::false_type {};
template <typename Target> struct contains<Target, Target> : std::true_type {};
template <typename Target, template <typename...> typename Templated, typename... Args> struct contains<Target, Templated<Args...>> : std::disjunction<contains<Target, Args>...> {};

// Check if a term contains a local variable
template <typename T> struct contains_local_variable : std::false_type {};
template <std::size_t Key, typename ScopeParam> struct contains_local_variable<variable<Key, local_scope<ScopeParam>>> : std::true_type {};
template <std::size_t Key> struct contains_local_variable<local<Key>> : std::true_type {};
template <template <typename...> typename Templated, typename... Args> struct contains_local_variable<Templated<Args...>> : std::disjunction<contains_local_variable<Args>...> {};

// Substitute every occurence of Pattern with Replacement
template <typename Pattern, typename Replacement, typename Data> struct substitute { using type = Data; };
template <typename Pattern, typename Replacement> struct substitute<Pattern, Replacement, Pattern> { using type = Replacement; };

template <typename Pattern, typename Replacement, template <typename...> typename Templated, typename... Args>
struct substitute<Pattern, Replacement, Templated<Args...>>
{
    // Covers both equations and predicates
    using type = Templated<typename substitute<Pattern, Replacement, Args>::type...>;
};

// Replace local variables with properly scoped variables
template <typename Data, typename Scope> struct scope_locals { using type = Data; };
template <std::size_t Key, typename Scope> struct scope_locals<local<Key>, Scope> { using type = variable<Key, Scope>; };
template <template <typename...> typename Templated, typename... Args, typename Scope> struct scope_locals<Templated<Args...>, Scope> { using type = Templated<typename scope_locals<Args, Scope>::type...>; };

// Possible unification actions (Martelli and Montanari, 1982)
enum class unification_action { DELETE, DECOMPOSE, SWAP, ELIMINATE };

// Test for valid actions
template <typename Action> constexpr bool is_valid_action(Action) { return std::is_same_v<Action, unification_action>; }

// Failure results
struct unification_failure {};
struct resolution_failure {};

// Unification rules (single step)
template <typename Equation, typename Remaining, typename Enable = void> struct step;

template <typename T, typename... Remaining>
struct step<equation<T, T>,
            set<Remaining...>,
            void>
{
    // DELETE - Remove equations of the form T = T.
    constexpr static unification_action action = unification_action::DELETE;
    using unified_type = set<Remaining...>; // The remaining equation set to check
    using substitutions = set<>; // The substitions that were made (as equations in the form Pattern = Replacement)
};

template <template <typename...> typename Predicate, typename... Args1, typename... Args2, typename... Remaining>
struct step<equation<Predicate<Args1...>, Predicate<Args2...>>,
            set<Remaining...>,
            std::enable_if_t<sizeof...(Args1) == sizeof...(Args2) && !std::is_same_v<Predicate<Args1...>, Predicate<Args2...>>>>
{
    // DECOMPOSE - Extract equations of the type f(T1, T2, ...) = f(S1, S2, ...) into the equations T1 = S1, T2 = S2, etc.
    // Requires that both predicates have the same name and argument count, and that DELETE does not match.
    constexpr static unification_action action = unification_action::DECOMPOSE;
    using unified_type = set<Remaining..., equation<Args1, Args2>...>;
    using substitutions = set<>;
};

template <template <typename...> typename Predicate, std::size_t Variable, typename Scope, typename... Args, typename... Remaining>
struct step<equation<Predicate<Args...>, variable<Variable, Scope>>,
            set<Remaining...>,
            void>
{
    // SWAP - Inverts equations of the form f(...) = T into T = f(...)
    constexpr static unification_action action = unification_action::SWAP;
    using unified_type = set<equation<variable<Variable, Scope>, Predicate<Args...>>, Remaining...>;
    using substitutions = set<>;
};

template <std::size_t Variable, typename Scope, typename Term, typename... Remaining>
struct step<equation<variable<Variable, Scope>, Term>,
            set<Remaining...>,
            std::enable_if_t<!contains<variable<Variable, Scope>, Term>::value>>
{
    // ELIMINATE - Replaces equations of the form V = T (where V is a variable) with the matching substitution.
    constexpr static unification_action action = unification_action::ELIMINATE;
    using unified_type = set<typename substitute<variable<Variable, Scope>, Term, Remaining>::type...>;
    using substitutions = set<equation<variable<Variable, Scope>, Term>>;
};

template <std::size_t Variable, typename Scope, std::size_t Symbol, typename... Remaining>
struct step<equation<symbol<Symbol>, variable<Variable, Scope>>,
            set<Remaining...>,
            void>
{
    // ELIMINATE (mirrored for symbols) - Replaces equations of the form s = V (where V is a variable and s a symbol) with the matching substitution V = s.
    // For the left type, predicates are already covered under SWAP and variables under ELIMINATE, so we only need to check for symbols.
    constexpr static unification_action action = unification_action::ELIMINATE;
    using unified_type = set<typename substitute<variable<Variable, Scope>, symbol<Symbol>, Remaining>::type...>;
    using substitutions = set<equation<variable<Variable, Scope>, symbol<Symbol>>>;
};

// Find a list of all matching substitutions for an equation set 
template <typename Equations, typename Substitutions, typename Enable = void> struct recursive { using substitutions = unification_failure; };
template <typename... Substitutions> struct recursive<set<>, set<Substitutions...>, void> { using substitutions = set<Substitutions...>; };

template <typename Front, typename... Remaining, typename... Substitutions>
struct recursive<set<Front, Remaining...>, set<Substitutions...>, std::enable_if_t<is_valid_action(step<Front, set<Remaining...>>::action)>>
{
    // Grab the first equation and attempt to unify it. (If this fails, the enable_if specialization
    // causes template substitution to fail, and the base case with unification_failure is used instead).
    using next_step = step<Front, set<Remaining...>>;
    
    // Grab the results
    using processed_equations = typename next_step::unified_type;
    using merged_substitutions = typename join<set<Substitutions...>, typename next_step::substitutions>::type;
    
    // Recurse
    using substitutions = typename recursive<processed_equations, merged_substitutions>::substitutions;
};

// Most General Unification: Starting at the end, apply all found substitutions to the right sides of previous substitutions, resulting in the most general unifier
template <typename Substitution, typename Equation> struct apply_substitution;

template <typename Pattern, typename Replacement, typename Left, typename Right>
struct apply_substitution<equation<Pattern, Replacement>, equation<Left, Right>>
{
    using type = equation<Left, typename substitute<Pattern, Replacement, Right>::type>;
};

template <typename Pattern, typename Replacement, typename... Equations>
struct apply_substitution<equation<Pattern, Replacement>, set<Equations...>>
{
    using type = set<typename apply_substitution<equation<Pattern, Replacement>, Equations>::type...>;
};

// Recursively apply all substitutions
template <typename SubstitutionSet> struct apply_all_substitutions;

template <> struct apply_all_substitutions<set<>> { using substitutions = set<>; };
template <> struct apply_all_substitutions<unification_failure> { using substitutions = unification_failure; };

template <typename... Substitutions>
struct apply_all_substitutions<set<Substitutions...>>
{
    // Get the current substitution
    using current_substitution = typename last<Substitutions...>::type;
    using remaining_substitutions = typename last<Substitutions...>::head;
    
    // Apply the current substitution
    using applied = typename apply_substitution<current_substitution, remaining_substitutions>::type;
    
    // Recurse
    using recursed = typename apply_all_substitutions<applied>::substitutions;
    
    // Append the current substitution
    using substitutions = typename join<recursed, set<current_substitution>>::type;
};

// Build Most General Unification for any equation set
template <typename EquationSet> struct unify;

template <typename... Equations>
struct unify<set<Equations...>>
{
    using any_unification = typename recursive<set<Equations...>, set<>>::substitutions;
    using substitutions = typename apply_all_substitutions<any_unification>::substitutions;
};

// Resolution: Determines what a particular variable is resolved to under a given unification.
// This also takes into account inverted equations (e.g. for X = Y, Y resolves to X).
template <typename Target, typename Substitutions> struct fetch;
template <typename Target> struct fetch<Target, unification_failure> { using type = resolution_failure; };
template <typename Target> struct fetch<Target, set<>> { using type = Target; };
template <typename Target, typename Equation, typename... Substitutions> struct fetch<Target, set<Equation, Substitutions...>> : fetch<Target, set<Substitutions...>> {};
template <typename Target, typename Term, typename... Substitutions> struct fetch<Target, set<equation<Target, Term>, Substitutions...>> { using type = Term; };
template <typename Target, typename Term, typename... Substitutions> struct fetch<Target, set<equation<Term, Target>, Substitutions...>> { using type = Term; };

// Replace every variable with its unified value
template <typename Data, typename Unified> struct bind_unified { using type = typename fetch<Data, Unified>::type; };
template <std::size_t Key, typename Scope, typename Unified> struct bind_unified<variable<Key, Scope>, Unified> { using type = typename fetch<variable<Key, Scope>, Unified>::type; };
template <template <typename...> typename Templated, typename... Args, typename Unified> struct bind_unified<Templated<Args...>, Unified> { using type = Templated<typename bind_unified<Args, Unified>::type...>; };

// A rule
template <typename DependentExpression, typename... LocalDeduction>
struct rule
{
    // Scope local variables
    using scope = local_scope<rule<DependentExpression, LocalDeduction...>>;
    using dependent_expression = typename scope_locals<DependentExpression, scope>::type;
    using local_deductions     = typename scope_locals<set<LocalDeduction...>, scope>::type;
    
    // Deduce local variables through the LocalDeduction equations
    using unification_results = typename unify<local_deductions>::substitutions;
    
    // Fill out the DependentExpression by replacing each local variable with its deduced value
    using next_expression = typename bind_unified<dependent_expression, unification_results>::type;
};

// Filter out rules that result in resolution failure
template <typename... Rules> struct filter_failures { using type = set<>; };

template <typename Rule, typename... Remainder>
struct filter_failures<Rule, Remainder...>
{
    // Check for resolution failure
    constexpr static bool failed = contains<resolution_failure, typename Rule::next_expression>::value;
    
    // Leave failed predicates out
    using prefix_type = std::conditional_t<failed, set<>, set<Rule>>;
    
    // Recurse
    using type = typename join<prefix_type, typename filter_failures<Remainder...>::type>::type;
};

// A collection of rules makes up our deduction rules
template <typename... Rules> struct rules { using valid = typename filter_failures<Rules...>::type; };

// Recursive evaluation of the deduction rules until success. Results in a set of possible results. Each possible result is itself a set of unification results (i.e. equations)
template <typename Rules, typename... UnificationResults> struct solve_helper : solve_helper<typename Rules::valid, UnificationResults...> {}; // Solve a block of rules by iterating over its valid members

template <typename... UnificationResults>
struct solve_helper<success, UnificationResults...>
{
    // Solve a fact by storing the unifications used to get there.
    using solutions = set<typename apply_all_substitutions<typename join<UnificationResults...>::type>::substitutions>;
};

template <typename... SingleRules, typename... UnificationResults>
struct solve_helper<set<SingleRules...>, UnificationResults...>
{
    // Solve a rule by solving its next_expression and storing the matching substitutions
    using solutions = typename join<set<>, typename solve_helper<typename SingleRules::next_expression, UnificationResults..., typename SingleRules::unification_results>::solutions...>::type;
};

// Filter local variables from the set of results
template <typename... Equations> struct filter_locals_helper;
template <> struct filter_locals_helper<set<>> { using type = set<>; };
template <typename Front, typename... Remainder>
struct filter_locals_helper<set<Front, Remainder...>>
{
    // Check for locals and skip Front if necessary
    constexpr static bool has_locals = contains_local_variable<Front>::value;
    using prefix_type = std::conditional_t<has_locals, set<>, set<Front>>;
    
    // Recurse
    using type = typename join<prefix_type, typename filter_locals_helper<set<Remainder...>>::type>::type;
};
template <typename Results> struct filter_locals;
template <typename... Result> struct filter_locals<set<Result...>> { using type = set<typename filter_locals_helper<Result>::type...>; }; // Runs filter_locals_helper on each set of equations in Results

// Recursive back-substitution to get the proper result
// Result is a set<...> of valid results. If it is empty, there is no valid solution.
// Otherwise, each valid result is a set<...> of variable assignments that fulfil the
// specified query
template <typename Query> using solve = typename filter_locals<typename solve_helper<Query>::solutions>::type;

} // namespace cex::template_logic

#endif // CEX_TEMPLATE_LOGIC_HPP_INCLUDED
